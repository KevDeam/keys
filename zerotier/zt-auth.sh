#!/bin/sh

NW_ID=
NW_TOKEN=

if [ -n "$NW_ID" ]; then 
  sleep 1; zerotier-cli join "${NW_ID}";
  MYID=$(zerotier-cli info|cut -d " " -f 3);
  echo "Join to ${NW_ID}, my ID: ${MYID}"
  while [ -z "$(zerotier-cli listnetworks | grep $NW_ID | grep ACCESS_DENIED)" ]; do echo "wait for connect"; sleep 1 ; done
  if [ -n "$NW_TOKEN" ]; then
    echo "Found ENV: NW_TOKEN, will auto auth myself ..."
    MYURL=https://my.zerotier.com/api/network/${NW_ID}/member/$MYID
    wget --header "Authorization: Bearer ${NW_TOKEN}" "${MYURL}" -q -O /tmp/ztinfo.txt
    sed \'s/"authorized":false/"authorized":true/\' /tmp/ztinfo.txt > /tmp/ztright.txt
    wget --header "Authorization: Bearer ${NW_TOKEN}" --post-data="$(cat /tmp/ztright.txt)" -q -O- "${MYURL}"
    rm /tmp/ztinfo.txt && rm /tmp/ztright.txt
    while [ -z "$(zerotier-cli listnetworks | grep $NW_ID | grep OK)" ]; do echo "wait for auth";sleep 1 ; done
  fi
fi