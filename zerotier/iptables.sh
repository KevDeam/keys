#!/bin/sh
NIC=

iptables -I FORWARD -i zt+ -j ACCEPT
iptables -I FORWARD -o zt+ -j ACCEPT
iptables -t nat -I POSTROUTING -o zt+ -j MASQUERADE
iptables -t nat -A POSTROUTING -o ${NIC} -j MASQUERADE
